﻿namespace Hont.AStar
{
    public interface IAStarHCost
    {
        int GetCost(Position currentPos, Position targetPos);
    }
}
